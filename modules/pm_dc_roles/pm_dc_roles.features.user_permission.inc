<?php
/**
 * @file
 * pm_dc_roles.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function pm_dc_roles_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'Project Management Issue: access'.
  $permissions['Project Management Issue: access'] = array(
    'name' => 'Project Management Issue: access',
    'roles' => array(
      'Client' => 'Client',
      'Developer' => 'Developer',
      'PM admin' => 'PM admin',
      'Tester' => 'Tester',
      'administrator' => 'administrator',
    ),
    'module' => 'pmissue',
  );

  // Exported permission: 'Project Management Organization: access'.
  $permissions['Project Management Organization: access'] = array(
    'name' => 'Project Management Organization: access',
    'roles' => array(
      'Client' => 'Client',
      'Developer' => 'Developer',
      'PM admin' => 'PM admin',
      'Project Manager' => 'Project Manager',
      'Tester' => 'Tester',
      'administrator' => 'administrator',
    ),
    'module' => 'pmorganization',
  );

  // Exported permission: 'Project Management Project: access'.
  $permissions['Project Management Project: access'] = array(
    'name' => 'Project Management Project: access',
    'roles' => array(
      'Client' => 'Client',
      'Developer' => 'Developer',
      'PM admin' => 'PM admin',
      'Project Manager' => 'Project Manager',
      'Tester' => 'Tester',
      'administrator' => 'administrator',
    ),
    'module' => 'pmproject',
  );

  // Exported permission: 'Project Management Task: access'.
  $permissions['Project Management Task: access'] = array(
    'name' => 'Project Management Task: access',
    'roles' => array(
      'PM admin' => 'PM admin',
      'administrator' => 'administrator',
    ),
    'module' => 'pmtask',
  );

  // Exported permission: 'Project Management Ticket: access'.
  $permissions['Project Management Ticket: access'] = array(
    'name' => 'Project Management Ticket: access',
    'roles' => array(
      'Client' => 'Client',
      'Developer' => 'Developer',
      'PM admin' => 'PM admin',
      'Tester' => 'Tester',
      'administrator' => 'administrator',
    ),
    'module' => 'pmticket',
  );

  // Exported permission: 'Project Management: access administration pages'.
  $permissions['Project Management: access administration pages'] = array(
    'name' => 'Project Management: access administration pages',
    'roles' => array(
      'PM admin' => 'PM admin',
      'administrator' => 'administrator',
    ),
    'module' => 'pm',
  );

  // Exported permission: 'Project Management: access dashboard'.
  $permissions['Project Management: access dashboard'] = array(
    'name' => 'Project Management: access dashboard',
    'roles' => array(
      'Client' => 'Client',
      'Developer' => 'Developer',
      'HR' => 'HR',
      'PM admin' => 'PM admin',
      'Project Manager' => 'Project Manager',
      'Tester' => 'Tester',
      'administrator' => 'administrator',
    ),
    'module' => 'pm',
  );

  return $permissions;
}
