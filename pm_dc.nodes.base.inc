<?php

/**
 * @file
 *  Migration for Drupal PM Organization nodes.
 */

/**
 * Implementation of ImportBaseNodes, to support migration of moderated nodes.
 */
class PMDCNodes extends ImportBaseNodes {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import Nodes.');
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'pm_dc_key' => array(
          'type' => 'char',
          'length' => 36,
          'not null' => FALSE,
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Create a MigrateSource object.
    $this->source = new MigrateSourceCSV(pm_dc_import_path($arguments['filename']), $this->csvcolumns(), array('header_rows' => 1));

    $this->removeFieldMapping('body:format');
    $this->addFieldMapping('body:format')->defaultValue('full_html');
    // Created
    $this->addFieldMapping('created', 'created')->defaultValue(strtotime("now"));

    $this->removeFieldMapping('uid', 'uid');
    $this->addfieldmapping('uid', 'author')->sourceMigration('PMDCUsers');
  }

  public function prepareRow($row) {
    parent::prepareRow($row);
  }
}

/**
 * Implementation of ImportBaseNodes, to support migration of moderated nodes.
 */
class PMDCNodesWithDate extends PMDCNodes {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->addfieldmapping('pm_date', 'date');
    $this->addfieldmapping('pm_date:to', 'date_to');
  }

  public function prepareRow($row) {
    parent::prepareRow($row);
    $date = explode(' to ', $row->date);
    $row->date =(string) isset($date[0]) ? strtotime($date[0]) : '';
    $row->date_to =(string) isset($date[1]) ? strtotime($date[1]) : '';
    return TRUE;
  }
}
