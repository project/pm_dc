<?php

/**
 * @file
 *  Migration for Drupal PM Organization nodes.
 */


/**
 * Implementation of ImportBaseNodes, to support migration of moderated nodes.
 */
class PMDCTimetrackings extends PMDCNodesWithDate {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $parent_source_class_name = $arguments['parent_source_class_name'];
    $this->description = t('Import Timetrackings.');
    $this->destination = new MigrateDestinationNode('pmtimetracking');
    $this->addfieldmapping('pm_billing_duration', 'billing_duration');
    $this->addfieldmapping('pm_billing_status', 'billing_status');
    $this->addfieldmapping('pm_duration', 'duration');
    $this->addfieldmapping('pm_durationunit', 'duration_unit');
    $this->addfieldmapping('pmtimetracking_parent', 'parent')->sourceMigration($parent_source_class_name);
  }

  function csvcolumns() {
    $columns[0] = array('pm_dc_key', 'pm_dc_key');
    $columns[1] = array('title', 'Name');
    $columns[2] = array('author', 'Author');
    $columns[3] = array('date', 'Date');
    $columns[4] = array('duration', 'Duration');
    $columns[5] = array('duration_unit', 'Duration Unit');
    $columns[6] = array('parent', 'Parent');
    $columns[7] = array('billing_duration', 'Billing Duration');
    $columns[8] = array('billing_status', 'Billing Status');
    $columns[9] = array('body', 'Note');
    return $columns;
  }

}
