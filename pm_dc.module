<?php

/**
 * @file
 * Code for the Drupal PM Demo feature.
 */

/**
 * Helper function to get path to folder where import data is stored.
 */
function pm_dc_import_path($filename) {
  $path = drupal_get_path('module', 'pm_dc') . '/import/' . $filename;
  return $path;
}

/**
 * Implements hook_migrate_api().
 */
function pm_dc_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'pm_dc' => array(
        'title' => t('Drupal Project Managment'),
      ),
    ),
    'migrations' => array(
      'PMDCUserPictures' => array(
        'class_name' => 'PMDCUserPictures',
        'filename' => 'pm_dc.users.csv',
        'group_name' => 'pm_dc',
      ),
      'PMDCUsers' => array(
        'class_name' => 'PMDCUsers',
        'filename' => 'pm_dc.users.csv',
        'picture_source' => 'PMDCUserPictures',
        'group_name' => 'pm_dc',
      ),
      'PMDCOrganizations' => array(
        'class_name' => 'PMDCOrganizations',
        'filename' => 'pm_dc.nodes.pmorganization.csv',
        'group_name' => 'pm_dc',
      ),
      'PMDCProjects' => array(
        'class_name' => 'PMDCProjects',
        'filename' => 'pm_dc.nodes.pmproject.csv',
        'group_name' => 'pm_dc',
      ),
      'PMDCTasks' => array(
        'class_name' => 'PMDCTasks',
        'filename' => 'pm_dc.nodes.pmtask.csv',
        'group_name' => 'pm_dc',
      ),
      'PMDCTickets' => array(
        'class_name' => 'PMDCTickets',
        'filename' => 'pm_dc.nodes.pmticket.csv',
        'group_name' => 'pm_dc',
      ),
      'PMDCIssues' => array(
        'class_name' => 'PMDCIssues',
        'filename' => 'pm_dc.nodes.pmissue.csv',
        'group_name' => 'pm_dc',
      ),
      'PMDCTimetrackingsProjects' => array(
        'class_name' => 'PMDCTimetrackings',
        'filename' => 'pm_dc.nodes.pmproject.pmtimetracking.csv',
        'parent_source_class_name' => 'PMDCProjects',
        'group_name' => 'pm_dc',
      ),
      'PMDCTimetrackingsIssues' => array(
        'class_name' => 'PMDCTimetrackings',
        'filename' => 'pm_dc.nodes.pmissue.pmtimetracking.csv',
        'parent_source_class_name' => 'PMDCIssues',
        'group_name' => 'pm_dc',
      ),
      'PMDCTimetrackingsTasks' => array(
        'class_name' => 'PMDCTimetrackings',
        'filename' => 'pm_dc.nodes.pmtask.pmtimetracking.csv',
        'parent_source_class_name' => 'PMDCTasks',
        'group_name' => 'pm_dc',
      ),
      'PMDCTimetrackingsTickets' => array(
        'class_name' => 'PMDCTimetrackings',
        'filename' => 'pm_dc.nodes.pmticket.pmtimetracking.csv',
        'parent_source_class_name' => 'PMDCTickets',
        'group_name' => 'pm_dc',
      ),
      'PMDCNotesOrganizations' => array(
        'class_name' => 'PMDCNotes',
        'filename' => 'pm_dc.nodes.pmorganization.pmnote.csv',
        'parent_source_class_name' => 'PMDCOrganizations',
        'group_name' => 'pm_dc',
      ),
      'PMDCNotesProjects' => array(
        'class_name' => 'PMDCNotes',
        'filename' => 'pm_dc.nodes.pmproject.pmnote.csv',
        'parent_source_class_name' => 'PMDCProjects',
        'group_name' => 'pm_dc',
      ),
      'PMDCNotesTasks' => array(
        'class_name' => 'PMDCNotes',
        'filename' => 'pm_dc.nodes.pmtask.pmnote.csv',
        'parent_source_class_name' => 'PMDCTasks',
        'group_name' => 'pm_dc',
      ),
      'PMDCNotesTickets' => array(
        'class_name' => 'PMDCNotes',
        'filename' => 'pm_dc.nodes.pmticket.pmnote.csv',
        'parent_source_class_name' => 'PMDCTickets',
        'group_name' => 'pm_dc',
      ),
      'PMDCNotesIssues' => array(
        'class_name' => 'PMDCNotes',
        'filename' => 'pm_dc.nodes.pmissue.pmnote.csv',
        'parent_source_class_name' => 'PMDCIssues',
        'group_name' => 'pm_dc',
      ),
      'PMDCExpensesOrganizations' => array(
        'class_name' => 'PMDCExpenses',
        'filename' => 'pm_dc.nodes.pmorganization.pmexpense.csv',
        'parent_source_class_name' => 'PMDCOrganizations',
        'group_name' => 'pm_dc',
      ),
      'PMDCExpensesProjects' => array(
        'class_name' => 'PMDCExpenses',
        'filename' => 'pm_dc.nodes.pmproject.pmexpense.csv',
        'parent_source_class_name' => 'PMDCProjects',
        'group_name' => 'pm_dc',
      ),
      'PMDCExpensesTasks' => array(
        'class_name' => 'PMDCExpenses',
        'filename' => 'pm_dc.nodes.pmtask.pmexpense.csv',
        'parent_source_class_name' => 'PMDCTasks',
        'group_name' => 'pm_dc',
      ),
      'PMDCExpensesTickets' => array(
        'class_name' => 'PMDCExpenses',
        'filename' => 'pm_dc.nodes.pmticket.pmexpense.csv',
        'parent_source_class_name' => 'PMDCTickets',
        'group_name' => 'pm_dc',
      ),
      'PMDCExpensesIssues' => array(
        'class_name' => 'PMDCExpenses',
        'filename' => 'pm_dc.nodes.pmissue.pmexpense.csv',
        'parent_source_class_name' => 'PMDCIssues',
        'group_name' => 'pm_dc',
      ),
      'PMDCPages' => array(
        'class_name' => 'PMDCPages',
        'filename' => 'pm_dc.nodes.pages.csv',
        'group_name' => 'pm_dc',
      ),
    ),
  );
  return $api;
}

/**
 * Implements hook_df_import_alter().
 */
function pm_dc_df_import_alter(&$migrations) {
  return $migrations['pm_dc'] = array(
    'PMDCUserPictures',
    'PMDCUsers',
    'PMDCOrganizations',
    'PMDCProjects',
    'PMDCTasks',
    'PMDCTickets',
    'PMDCIssues',
    'PMDCTimetrackingsProjects',
    'PMDCTimetrackingsIssues',
    'PMDCTimetrackingsTasks',
    'PMDCTimetrackingsTickets',
    'PMDCNotesOrganizations',
    'PMDCNotesProjects',
    'PMDCNotesTasks',
    'PMDCNotesTickets',
    'PMDCNotesIssues',
    'PMDCExpensesOrganizations',
    'PMDCExpensesProjects',
    'PMDCExpensesTasks',
    'PMDCExpensesTickets',
    'PMDCExpensesIssues',
    'PMDCPages',
  );
}
