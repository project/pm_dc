api = 2
core = 7.x

; Download the DF install profile and recursively build all its dependencies.
projects[] = df

; Contrib
projects[pm][version] = 3.x-dev
projects[pm][type] = "module"
projects[pm][subdir] = "contrib"
projects[pm][download][type] = "git"
projects[pm][download][branch] = "7.x-3.x"

projects[] = email
projects[] = fontawesome
projects[] = fontawesome_iconpicker

; Libraries
libraries[fontawesome_iconpicker][download][type] = "get"
libraries[fontawesome_iconpicker][download][url] = "https://github.com/mjolnic/fontawesome-iconpicker/archive/1.0.0.zip"
libraries[fontawesome_iconpicker][directory_name] = "fontawesome-iconpicker"
libraries[fontawesome_iconpicker][destination] = "libraries"

libraries[fontawesome][download][type] = "get"
libraries[fontawesome][download][url] = "https://github.com/FortAwesome/Font-Awesome/archive/master.zip"
libraries[fontawesome][directory_name] = "fontawesome"
libraries[fontawesome][destination] = "libraries"

; Themes
projects[bootstrap][version] = "3.5"
projects[bootstrap][type] = "theme"
projects[bootstrap][subdir] = "contrib"
projects[bootstrap][download][type] = "git"
projects[bootstrap][download][branch] = "7.x-3.5"

projects[pm_kickstart_theme][version] = "3.x-dev"
projects[pm_kickstart_theme][type] = "theme"
projects[pm_kickstart_theme][subdir] = "contrib"
projects[pm_kickstart_theme][download][type] = "git"
projects[pm_kickstart_theme][download][branch] = "7.x-3.x"
