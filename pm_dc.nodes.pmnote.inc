<?php

/**
 * @file
 *  Migration for Drupal PM Notes.
 */


/**
 * Implementation of PMDCNodes, to support migration of Notes.
 */
class PMDCNotes extends PMDCNodes {

  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import Notes.');
    $this->destination = new MigrateDestinationNode('pmnote');
    $this->addfieldmapping('pmnote_parent', 'parent')->sourceMigration($arguments['parent_source_class_name']);
  }

  function csvcolumns() {
    $columns[0] = array('pm_dc_key', 'pm_dc_key');
    $columns[1] = array('title', 'Name');
    $columns[2] = array('author', 'Author');
    $columns[3] = array('parent', 'Parent');
    $columns[4] = array('body', 'Note');
    return $columns;
  }

}
