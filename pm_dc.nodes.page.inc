<?php

/**
 * @file
 *  Migration for Pages.
 */


/**
 * Implementation of PMDCPages, to support migration of Pages.
 */
class PMDCPages extends PMDCNodes {

  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import Pages.');
    $this->destination = new MigrateDestinationNode('page');
  }

  function csvcolumns() {
    $columns[0] = array('pm_dc_key', 'pm_dc_key');
    $columns[1] = array('title', 'Name');
    $columns[2] = array('author', 'Author');
    $columns[3] = array('body', 'Body');
    return $columns;
  }

}
