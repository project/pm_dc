<?php

/**
 * @file
 *  Migration for Commerce Products used in PM DC.
 */

class PMDCUsers extends ImportBaseUsers {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $arguments['path'] = pm_dc_import_path($arguments['filename']);
    $import_path = $arguments['path'];
    $this->source = new MigrateSourceCSV($import_path, $this->csvcolumns(), array('header_rows' => 1));
    $picture_source = isset($arguments['picture_source']) ? $arguments['picture_source'] : 'PMDCUserPictures';

    $this->removeFieldMapping('picture', 'picture');
    $this->addFieldMapping('picture', 'picture')->sourceMigration($picture_source);
    // Last login
    $this->addFieldMapping('login', 'access')->defaultValue(strtotime("-1 week"));

    $this->removeFieldMapping('login', 'login');
    $this->addFieldMapping('login', 'login')->defaultValue(strtotime("-1 week"));

    $this->description = t('Import Users.');
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'pm_dc_key' => array(
          'type' => 'char',
          'length' => 36,
          'not null' => FALSE,
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );
  }

  function csvcolumns() {
    $columns[0] = array('pm_dc_key', 'pm_dc_key');
    $columns[1] = array('name', 'name');
    $columns[2] = array('pass', 'pass');
    $columns[3] = array('mail', 'mail');
    $columns[4] = array('status', 'status');
    $columns[5] = array('roles', 'roles');
    $columns[6] = array('picture', 'picture');
    return $columns;
  }

  public function prepareRow($row) {
    $user_roles = explode(", ", $row->roles);
    $roles = array('2' => '2');
    foreach ($user_roles as $role_name) {
      $rid = db_query('SELECT rid FROM {role} WHERE name = :name', array(':name' => $role_name))->fetchField();
      $roles[$rid] = $rid;
    }
    $row->roles = $roles;
    $row->interests = explode(", ", $row->interests);
    // Convert date string in csv to a datetime
    if (isset($row->login)) {
      $row->login = strtotime($row->login);
    }
    return TRUE;
  }
}


class PMDCUserPictures extends ImportBaseUserPictures {
  public function __construct($arguments) {
    $import_path = $arguments['path'] = pm_dc_import_path($arguments['filename']);
    parent::__construct($arguments);
    $this->source = new MigrateSourceCSV($import_path, $this->csvcolumns(), array('header_rows' => 1));
    // Get the base path, which is where we assume /images is located
    $base_path = dirname($import_path);

    $this->removeFieldMapping('source_dir');
    $this->addFieldMapping('source_dir')->defaultValue($base_path . '/images');
  }

  function csvcolumns() {
    $columns[6] = array('picture', 'Picture');
    return $columns;
  }
}
