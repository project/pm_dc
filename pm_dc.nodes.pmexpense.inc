<?php

/**
 * @file
 *  Migration for Drupal PM Organization nodes.
 */


/**
 * Implementation of PMDCExpenses, to support migration of moderated nodes.
 */
class PMDCExpenses extends PMDCNodes {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $parent_source_class_name = $arguments['parent_source_class_name'];
    $this->description = t('Import Timetrackings.');
    $this->destination = new MigrateDestinationNode('pmexpense');
    $this->addfieldmapping('pm_amount', 'amount');
    $this->addfieldmapping('pm_provider', 'provider')->sourceMigration('PMDCOrganizations');
    $this->addfieldmapping('pmexpense_parent', 'parent')->sourceMigration($parent_source_class_name);
    $this->addfieldmapping('pm_billing_status', 'billing_status');
    $this->addfieldmapping('pmexpense_date', 'expense_date');
  }

  function csvcolumns() {
    $columns[0] = array('pm_dc_key', 'pm_dc_key');
    $columns[1] = array('title', 'Name');
    $columns[2] = array('author', 'Author');
    $columns[3] = array('parent', 'Parent');
    $columns[4] = array('amount', 'Amount');
    $columns[5] = array('expense_date', 'Expense Date');
    $columns[6] = array('provider', 'Provider');
    $columns[7] = array('billing_status', 'Billing Status');
    return $columns;
  }

  public function prepareRow($row) {
    parent::prepareRow($row);
    $row->date = (strtotime($date)) ? strtotime($date) : '';
    return TRUE;
  }
}
